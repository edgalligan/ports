#!/usr/bin/env bash

publish_dir="../tests/mine"

pushd "$( dirname -- "${BASH_SOURCE[0]}")/../repo" || exit 1
tar -cf "$publish_dir/ports.tar" .
#openssl dgst -ripemd160 -sign ${PRIVKEY} -out "$publish_dir/ports.tar.rmd160" "$publish_dir/ports.tar"
openssl dgst -ripemd160 -out "$publish_dir/ports.tar.rmd160" "$publish_dir/ports.tar"
