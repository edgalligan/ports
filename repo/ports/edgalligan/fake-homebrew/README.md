## Fake Homebrew

Small util to improve compat. with shell scripts that assume Homebrew presence.

### Goals

- Do nothing if Homebrew is installed - don't interfere
- Create a `brew` alias in user's `PATH` (requires use action to enable) that
  takes a small subset of commonly used Homebrew commands & responds with
  approximations of the intent OR with some user-friendly messaging.
- Provide some limited mapping of Homebrew -> equivalent MacPorts package
  names, for a small subset of common packages with different aliases
- Err on the side of safety & stability when providing approximate
  functionality: if something is likely to break things don't alias it
